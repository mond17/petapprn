'use strict';

import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const genericTextShadow = {
  textShadowColor: 'rgba(0,0,0,0.5)',
  textShadowOffset: {
    width: 2,
    height: 2,
  },
  textShadowRadius: 2,
};

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  card: {
    flex: 1,
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#999',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent',
  },
  /* + + + + + + + + + + + + + + + + + + + + + 
    // General
    + + + + + + + + + + + + + + + + + + + + + */
  TextDefaultCenter: {
    textAlign: 'center',
    color: '#000',
    fontSize: 14,
    lineHeight: 24,
  },
  TextBoldCenter: {
    textAlign: 'center',
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  TextBoldLeft: {
    textAlign: 'left',
    color: '#000',
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  TextBoldLeftBigger: {
    textAlign: 'left',
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  TextDefaultCenterBigger: {
    textAlign: 'center',
    color: '#000',
    fontSize: 16,
    lineHeight: 22,
  },
  TextBoldCenterBigger: {
    textAlign: 'center',
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  TextBoldCenterBigger2: {
    textAlign: 'center',
    color: '#000',
    fontSize: 28,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  TextBoldCenterBigger2: {
    textAlign: 'center',
    color: '#000',
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  Divider: {
    color: '#000',
    marginTop: 10,
    marginBottom: 10,
  },
  /* + + + + + + + + + + + + + + + + + + + + + 
    // Swipable
    + + + + + + + + + + + + + + + + + + + + + */
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  standalone: {
    marginTop: 30,
    marginBottom: 30,
  },
  standaloneRowFront: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    justifyContent: 'center',
    height: 50,
  },
  standaloneRowBack: {
    alignItems: 'center',
    backgroundColor: '#8BC645',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
  },
  backTextWhite: {
    color: '#FFF',
  },
  rowFront: {
    // alignItems: 'flex-start',
    backgroundColor: '#fff',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    // justifyContent: 'center',
    height: 100,
  },
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
  },
  backRightBtnLeft: {
    backgroundColor: 'blue',
    right: 75,
  },
  backRightBtnRight: {
    backgroundColor: 'red',
    right: 0,
  },
  controls: {
    alignItems: 'center',
    marginBottom: 30,
  },
  switchContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 5,
  },
  switch: {
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
    paddingVertical: 10,
    width: Dimensions.get('window').width / 4,
  },
  trash: {
    height: 25,
    width: 25,
  },
  /* + + + + + + + + + + + + + + + + + + + + + 
    // General
    + + + + + + + + + + + + + + + + + + + + + */
  Button_Icon: {
    width: 22,
    height: 35,
  },
  ButtonTest: {
    alignItems: 'center',
    backgroundColor: '#999999',
    padding: 10,
    margin: 10,
  },
  Screen: {
    flex: 1,
    backgroundColor: '#0e1221',
    color: 'white',
  },
  DefaultText: {
    color: '#ffffff',
    fontSize: 24,
  },
  SubText: {
    color: '#dbdbdb',
    fontSize: 24,
  },
  /* + + + + + + + + + + + + + + + + + + + + + 
    // NEW
    + + + + + + + + + + + + + + + + + + + + + */
  Responsive_Image: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined,
    margin: 40,
  },
  Barlow_Semibold: {
    fontFamily: 'Barlow-SemiBold',
    textAlign: 'center',
    color: '#a7b6dd',
    fontSize: 20,
    lineHeight: 28,
  },
  Barlow_Light: {
    fontFamily: 'Barlow-Light',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 16,
    lineHeight: 24,
  },
  Barlow_Semibold_Left: {
    fontFamily: 'Barlow-SemiBold',
    textAlign: 'left',
    color: '#a7b6dd',
    fontSize: 20,
    lineHeight: 28,
  },
  Barlow_Light_Left: {
    fontFamily: 'Barlow-Light',
    textAlign: 'left',
    color: '#ffffff',
    fontSize: 16,
    lineHeight: 24,
  },
  Barlow_Helper: {
    fontFamily: 'Barlow-Light',
    textAlign: 'center',
    color: 'grey',
    fontSize: 26,
    lineHeight: 24,
  },
  buttonDefault: {
    backgroundColor: 'transparent',
    borderColor: '#de1f4e',
    borderWidth: 2,
    borderRadius: 8,
  },
  progressBar: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: '#a7b6dd',
    marginLeft: 2,
    marginRight: 2,
  },
  progressBarCurrent: {
    width: 20,
    height: 10,
    borderRadius: 10,
    backgroundColor: '#a7b6dd',
    marginLeft: 2,
    marginRight: 2,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
  textLogo: {
    padding: 10,

    // alignSelf: 'center',
    fontFamily: 'afternoon_in_stereo_personal_use',
    fontSize: 70,
    marginTop: 290,
    position: 'absolute',
  },
});
