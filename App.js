import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import {
  createAppContainer,
  createSwitchNavigator,
  SafeAreaView,
} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {
  createDrawerNavigator,
  DrawerNavigatorItems,
} from 'react-navigation-drawer';
// import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import {
  Header,
  Icon,
  Button,
  Divider,
  Overlay,
  Input,
  Avatar,
} from 'react-native-elements';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import * as Services from './services/Firebase_Services';

import styles from './styles/Stylesheet';

//screens
import HomeScreen from './screens/HomeScreen';
import RegisterScreen from './screens/RegisterScreen';
import LogInScreen from './screens/LogInScreen';
import ProfileScreen from './screens/ProfileScreen';
import CardScreen from './components/CardScreen';
import ExploreMainScreen from './screens/ExploreMainScreen';
import MessageScreen from './screens/MessageScreen';
import ChatListScreen from './screens/ChatListScreen';
import {useNavigation} from '@react-navigation/native';

import auth from '@react-native-firebase/auth';

function SplashScreen() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const navigation = useNavigation();
  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    navigation.navigate('LoginScreen');
    return;
    // return (
    //   <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
    //     <Text style={{alignSelf: 'center'}}>Login</Text>

    //     <TouchableOpacity
    //       style={{
    //         width: 300,
    //         height: 40,
    //         alignSelf:"center",
    //         backgroundColor: 'lightseagreen',
    //         borderRadius: 10,
    //       }}
    //       onPress={() => {
    //         Services.SignInUser();
    //       }}>
    //       <Text
    //         style={{
    //           fontSize: 20,
    //           fontWeight: 'bold',
    //           color: 'white',
    //           alignSelf: 'center',
    //         }}>
    //         LOG IN
    //       </Text>
    //     </TouchableOpacity>
    //   </View>
    // );
  } else navigation.navigate('HomeScreen');
  return;
  // return (
  //   <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
  //     <Text style={{alignSelf: 'center'}}>Welcome {user.email}</Text>

  //     <TouchableOpacity
  //       style={{
  //         width: 300,
  //         height: 40,
  //         height: 50,
  //         alignSelf: 'center',
  //         backgroundColor: 'lightseagreen',
  //         borderRadius: 10,
  //       }}
  //       onPress={() => {
  //         Services.LogoutUser();
  //       }}>
  //       <Text
  //         style={{
  //           fontSize: 20,
  //           fontWeight: 'bold',
  //           color: 'white',
  //           alignSelf: 'center',
  //         }}>
  //         Log out
  //       </Text>
  //     </TouchableOpacity>
  //   </View>
  // );
}

class GlobalModal extends React.Component {
  render() {
    return (
      <View style={{flex: 1, position: 'absolute'}}>
        <Overlay
          isVisible={this.props.isVisible}
          onBackdropPress={this.props.onBackdropPress}>
          <View style={{flex: 1}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={styles.TextBoldLeft}>Global Modal!</Text>
              <Icon
                type="material"
                name="close"
                size={30}
                onPress={this.props.onClosePress}
              />
            </View>
          </View>
        </Overlay>
      </View>
    );
  }
}

class TempScreen extends React.PureComponent {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
          <Text>Feature coming soon!</Text>
        </View>
      </View>
    );
  }
}

const navOps = ({navigation, screenProps}) => ({
  headerLeft: (
    <TouchableHighlight
      style={{margin: 20}}
      onPress={() => {
        navigation.toggleDrawer();
      }}>
      <Icon type="material" name="account-circle" color="#000" size={30} />
    </TouchableHighlight>
  ),
});

//Tab Navigator for Home

const tabBarIconSize = 30;
const TabNavigator = createBottomTabNavigator(
  // const TabNavigator = createMaterialBottomTabNavigator(
  {
    HomeScreen: {
      screen: createStackNavigator({
        HomeScreen: {
          screen: HomeScreen,
          navigationOptions: {
            header: null,
          },
        },
        ProfileScreen: {
          screen: ProfileScreen,
          navigationOptions: {
            header: null,
          },
        },
      }),

      navigationOptions: {
        headerStyle: {
          backgroundColor: 'lightseagreen',
        },

        title: 'Home',
        tabBarIcon: ({focused, tintColor}) => (
          <Icon
            type="material"
            name="dns"
            size={tabBarIconSize}
            color={tintColor}
          />
        ),
      },
    },
    ExploreScreen: {
      screen: createStackNavigator({
        ExploreMainScreen: {
          screen: ExploreMainScreen,
          navigationOptions: {
            header: null,
          },
        },
        ExploreSubScreen: {
          screen: TempScreen,
          navigationOptions: {
            header: null,
          },
        },
      }),
      navigationOptions: {
        headerStyle: {
          backgroundColor: 'lightseagreen',
        },
        headerTintColor: 'white',

        title: 'Home',
        tabBarIcon: ({focused, tintColor}) => (
          <Icon
            type="material"
            name="explore"
            size={tabBarIconSize}
            color={tintColor}
          />
        ),
      },
    },
    ChatListScreen: {
      screen: ChatListScreen,
      navigationOptions: {
        headerStyle: {
          backgroundColor: 'lightseagreen',
        },
        headerTintColor: 'white',

        title: 'Home',
        tabBarIcon: ({focused, tintColor}) => (
          <Icon
            type="font-awesome"
            name="comments"
            size={tabBarIconSize}
            color={tintColor}
          />
        ),
      },
    },
    // NotificationsScreen: {
    //   screen: TempScreen,
    //   navigationOptions: {
    //     headerStyle: {
    //       backgroundColor: 'lightseagreen',
    //     },
    //     headerTintColor: 'white',

    //     title: 'Home',
    //     tabBarIcon: ({focused, tintColor}) => (
    //       <Icon
    //         type="material"
    //         name="redeem"
    //         size={tabBarIconSize}
    //         color={tintColor}
    //       />
    //     ),
    //   },
    // },
  },
  //

  {
    initialRouteName: 'HomeScreen',
    lazy: false,
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#009c9e',
      // style:{height:100},
    },
    resetOnBlur: true,
  },
);

const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={{
        flex: 1,
        //  backgroundColor: '#009c9e'
      }}
      forceInset={{top: 'always', horizontal: 'never'}}>
      <View style={{padding: 10, textAlign: 'center'}}>
        {/* <Icon type="material" name="account-circle" size={80} /> */}

        <View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <Avatar
                containerStyle={{alignSelf: 'center', marginBottom: 5}}
                rounded
                size={100}
                source={{
                  uri: Services.userData.profile_photo,
                }}
                renderPlaceholderContent={
                  <Icon
                    type="material"
                    name="account-box"
                    color="#000"
                    // size={60}
                  />
                }
              />
            </View>
            <View
              style={{
                flex: 2,
                alignContent: 'center',
                alignSelf: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.TextBoldCenter}>
                {Services.userData.first_name +
                  ' ' +
                  Services.userData.last_name}
              </Text>
              <Text style={{textAlign: 'center'}}>
                {'@' + Services.userData.handle_name}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <Divider style={{backgroundColor: '#000', height: 2}} />
      <DrawerNavigatorItems {...props} />
      <View style={{padding: 4}}>
        <Button
          title="Logout"
          onPress={() => {
            props.navigation.navigate('LogInScreen');
          }}
        />
        <View style={{height: 4}} />
        <Button
          buttonStyle={{backgroundColor: 'red'}}
          title="Delete Account"
          onPress={() => {
            alert('Are you sure you want to delete your account?');
            // props.navigation.navigate("LandingScreen");
          }}
        />
      </View>
    </SafeAreaView>
  </ScrollView>
);

//Draw Navigator for Main Screen
const DrawNavigator = createDrawerNavigator(
  {
    HomeScreen: {
      screen: TabNavigator,
      navigationOptions: {
        title: 'Home',
        drawerIcon: ({focused}) => <Icon type="material" name="home" />,
      },
    },
    ProfileScreen: {
      screen: ProfileScreen,
      navigationOptions: {
        header: null,
        title: 'View Profile',
        drawerIcon: ({focused, tintColor}) => (
          <Icon type="font-awesome" name="id-card" />
        ),
      },
    },
    MessageScreen: {
      screen: MessageScreen,
      navigationOptions: {
        header: null,
        title: 'Notification',
        tabBarIcon: ({focused, tintColor}) => (
          <Icon
            type="font-awesome"
            name="bell"
            size={tabBarIconSize}
            color={tintColor}
          />
        ),
      },
    },
  },
  {
    initialRouteName: 'HomeScreen',
    contentComponent: CustomDrawerContentComponent,
    // drawerLockMode: 'unlocked',
    drawerPosition: 'right',
  },
);

const AppNavigator = createSwitchNavigator(
  {
    splashFlow: {
      screen: SplashScreen,
    },

    onBoardingFlow: {
      screen: createStackNavigator({
        LogInScreen: {
          screen: LogInScreen,
          navigationOptions: {
            header: null,
          },
        },

        RegisterScreen: {
          screen: RegisterScreen,
          navigationOptions: {
            headerStyle: {
              backgroundColor: 'lightseagreen',
            },
            headerTintColor: 'white',
          },
        },
        ProfileScreen: {
          screen: ProfileScreen,
          navigationOptions: {
            header: null,
          },
        },
      }),
    },

    mainFlow: {
      screen: createStackNavigator({
        MainScreen: {
          screen: DrawNavigator,
          navigationOptions: {
            header: null,
          },
        },
        ProfileScreen: {
          screen: ProfileScreen,
          navigationOptions: {
            header: null,
          },
        },
      }),
    },
  },
  {
    // initialRouteName: 'mainFlow',
    initialRouteName: 'splashFlow',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {modalVisible: false};
  }

  componentDidMount() {
    // Services.globalModal = this;
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <GlobalModal
          isVisible={this.state.modalVisible}
          onBackdropPress={() => {
            this.setState({modalVisible: false});
          }}
          onClosePress={() => {
            this.setState({modalVisible: false});
          }}
        />
        <AppContainer />
      </View>
    );
  }
}

console.disableYellowBox = true;
