'user strict';

import {Linking, Platform, Alert} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';

import users from '../mock_data/users.json';
// import companies from "../mockdata/companies.json";
import quotes from '../mock_data/quotes.json';

export var userData = users.splice(0, 1)[0];

//NetInfo
export const internet = {connected: '', connectionType: ''};

export function updateApp() {
  console.log('TABBAR', global.globalTabBar);
  global.globalTabBar ? global.globalTabBar.setNewNotif(true) : '';
}

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function TextAbstract(text, length) {
  if (text == null) {
    return '';
  }
  if (text.length <= length) {
    return text;
  }
  text = text.substring(0, length);
  last = text.lastIndexOf(' ');
  text = text.substring(0, last);
  return text + '...';
}

export function prepDummyData(data, count) {
  const feed = [];

  for (let i = 0; i < count; i++) {
    const pa = {};

    const u = data.splice(getRandomInt(0, data.length - 1), 1)[0];

    u.profile_photo =
      u.profile_photo.substring(0, u.profile_photo.indexOf('set=')) +
      'set=set' +
      getRandomInt(1, 4);

    pa.key = String(u.id);
    pa.profile_photo = u.profile_photo;
    pa.name = u.first_name + ' ' + u.last_name;
    pa.handle = '@' + u.handle_name;
    pa.likes = getRandomInt(1, 100);
    pa.comments = getRandomInt(1, 100);
    pa.content = u.sample_buzz;
    if (getRandomInt(0, 100) > 40) {
      pa.image =
        'https://picsum.photos/id/' + getRandomInt(0, 100) + '/100/100';
    }
    feed.push(pa);
  }

  return feed;
}

const unsubscribe = NetInfo.addEventListener(state => {
  console.log('Connection type', state.type);
  console.log('Is connected?', state.isConnected);
  internet.connected = state.isConnected;
  internet.connectionType = state.type;
});

export function getGPS() {
  console.log('attempting to get gps');
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      position => {
        coords = position.coords;
        resolve(position);
      },
      error => {
        reject(error);
      },
      // { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
      // { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 },
      {enableHighAccuracy: false, timeout: 5000},
    );
  });
}

export function saveData(key, data, callBack) {
  (async () => {
    try {
      const value = await AsyncStorage.setItem(
        '@' + key + ':key',
        JSON.stringify(data),
      );
      callBack({status: 'success', message: 'Saving Success!'});
    } catch (error) {
      callBack(error);
    }
  })();
}

export function loadData(key, callBack) {
  (async () => {
    //console.log("Loading...");
    try {
      const value = await AsyncStorage.getItem('@' + key + ':key');
      if (value !== null) {
        callBack(JSON.parse(value));
      } else {
        callBack({
          status: 'error',
          message:
            'Data is not available offline. Please connect to the Internet retrieve data.',
        });
      }
    } catch (error) {
      // Error retrieving data
      //console.error(error);
      callBack({
        status: 'error',
        message:
          'Data is not available offline. Please connect to the Internet to retrieve data.',
      });
    }
  })();
}

//new saving methods
export const storeData = async (key, data) => {
  try {
    await AsyncStorage.setItem('@' + key + ':key', JSON.stringify(data));
    return {status: 'success', reason: 'data saved'};
  } catch (error) {
    return error;
    // Error saving data
  }
};

export const retrieveData = async key => {
  try {
    const value = await AsyncStorage.getItem('@' + key + ':key');
    console.log('VALUE:', value);
    if (value !== null) {
      return JSON.parse(value);
    } else {
      return {status: 'failed', reason: 'data does not exist'};
    }
  } catch (error) {
    return error;
    // Error retrieving data
  }
};

//mock api
export function getUsers() {
  return new Promise((resolve, reject) => {
    // setTimeout(() => {
    resolve([...users]);
    // }, getRandomInt(5, 10) * 1000);
  });
}

export function getCompanies() {
  return new Promise((resolve, reject) => {
    // setTimeout(() => {
    resolve([...companies].splice(0, 10));
    // }, getRandomInt(5, 10) * 1000);
  });
}

export function getQuotes() {
  return [...quotes];
}
