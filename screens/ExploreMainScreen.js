'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  TextInput,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Icon,
  Header,
  Test,
} from 'react-native-elements';
import styles from '../styles/Stylesheet';

import NumberInput from '../components/NumberInput';

import SearchCard from '../components/SearchCard';
import ScalableText from 'react-native-text';
import * as Services from '../services/Services';
const data = [
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Pet shop',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Pet Clinic',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Vets.',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something four',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something five',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something six',
  },
];
const data2 = [
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Imperial Vet Clinic',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'Tanza Pet Shop',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'John Doe',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something four',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something five',
  },
  {
    imageUrl:
      'https://picsum.photos/id/' + Services.getRandomInt(0, 100) + '/100/100',
    title: 'something six',
  },
];
class ExploreMainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      data: data,
      data2: data2,
    };
  }

  componentDidMount() {
    // console.log('HEREEEEEEEEE!');
    // Services.getUsers().then(res => {
    //   this.users = res;
    //   const newData = Services.prepDummyData(this.users, 10);
    //   this.setState({listViewData: newData});
    //   console.log('popopopop ETO NA', this.state.listViewData);
    // });
  }
  render() {
    // const array = this.state.listViewData;
    // console.log('wowowow ETO NA', array);
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#009c9e" barStyle="light-content" />
        <View style={{flex: 1}}>
          <View style={{height: 20}}></View>
          <View
            style={{
              //   backgroundColor: 'red',
              backgroundColor: 'white',
              width: '88%',
              borderRadius: 5,
              elevation: 10,
              //   padding: 5,
              alignSelf: 'center',
              //   justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <Icon
              // iconStyle

              containerStyle={{padding: 10}}
              type="font-awesome"
              name="search"
              color="gray"
              size={20}></Icon>
            <TextInput
              placeholder="Search something"
              underlineColorAndroid="transparent"></TextInput>
          </View>
          <View style={{height: 20}}></View>
          <View style={{padding: 20}}>
            <Text style={styles.TextBoldLeftBigger}>
              What can we help you find, Mond?
            </Text>
          </View>
          {/* <ScrollView> */}
          {/* <View style={{height: 10}}></View> */}

          <View style={{padding: 10}}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.data}
              renderItem={({item: rowData}) => {
                return (
                  <View padding={10}>
                    <View
                      style={{
                        backgroundColor: 'white',

                        width: 120,
                        height: 150,
                        borderRadius: 5,
                        elevation: 10,
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <View style={{flex: 4}}>
                        <Image
                          borderTopLeftRadius={5}
                          borderTopRightRadius={5}
                          source={{uri: rowData.imageUrl}}
                          //   resizeMode="contain"
                          style={{width: '100%', height: '100%'}}
                        />
                      </View>
                      <Text
                        style={{
                          flex: 1,
                          marginBottom: 10,
                          padding: 10,
                          fontWeight: 'bold',
                        }}>
                        {rowData.title}
                      </Text>
                    </View>
                    {/* <View style={{height: 20}}></View> */}
                  </View>
                );
              }}
              keyExtractor={(item, index) => index}
            />
          </View>
          {/* <View style={{height: 20}}></View> */}
          {/* </ScrollView> */}

          <View style={{padding: 20}}>
            <Text style={styles.TextBoldLeftBigger}>Stores near you</Text>
          </View>
          {/* <ScrollView> */}
          <View style={{height: 10}}></View>

          <View style={{padding: 10}}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.data2}
              renderItem={({item: rowData}) => {
                return (
                  <View padding={10}>
                    <View
                      style={{
                        backgroundColor: 'white',

                        width: 100,
                        height: 150,
                        borderRadius: 5,
                        elevation: 10,
                        alignSelf: 'center',
                        justifyContent: 'center',
                      }}>
                      <View style={{flex: 4}}>
                        <Image
                          borderTopLeftRadius={5}
                          borderTopRightRadius={5}
                          source={{uri: rowData.imageUrl}}
                          //   resizeMode="contain"
                          style={{width: '100%', height: '100%'}}
                        />
                      </View>
                      <Text
                        style={{
                          flex: 1,
                          marginBottom: 10,
                          padding: 10,
                          fontWeight: 'bold',
                        }}>
                        {rowData.title}
                      </Text>
                    </View>
                  </View>
                );
              }}
              keyExtractor={(item, index) => index}
            />
          </View>
          <View style={{height: 20}}></View>
        </View>
      </View>
    );
  }
}

export default ExploreMainScreen;
