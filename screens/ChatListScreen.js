'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  ScrollView,
  TextInput,
  FlatList,
} from 'react-native';
import {
  Header,
  Icon,
  ListItem,
  Button,
  SearchBar,
  Avatar,
} from 'react-native-elements';
// import {createBottomTabNavigator} from 'react-navigation';
// import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import * as Services from '../services/Services';

import styles from '../styles/Stylesheet';
// import {FlatList} from 'react-native-gesture-handler';

// const list = [
//   {
//     name: "Amy Farha",
//     avatar_url:
//       "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg",
//     content: "See you tommorow."
//   },
//   {
//     name: "Chris Jackson",
//     avatar_url:
//       "https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg",
//     content:
//       "Any other additional info which would help us debug the issue quicker."
//   },
//   {
//     name: "Donald Trump",
//     avatar_url:
//       "https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg",
//     content: "The project work fine in dev mode when I run expo start."
//   }
// ];

class ChatListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {search: '', listViewData: []};
  }

  componentDidMount() {
    //load saved data first

    Services.getUsers().then(res => {
      const quotes = Services.getQuotes();
      const data = Services.prepDummyData(res, quotes.length - 1);

      data.map((l, i) => {
        l.content = quotes[i].quote;
      });
      console.log('joke', data);
      this.setState({listViewData: data});
    });
  }

  _onPressButton = () => {
    console.log('pressed!');
    alert('pressed!');
  };

  render() {
    const listViewData = this.state.listViewData;
    console.log('sad', listViewData);
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View style={{height: 20}}></View>
          <View
            style={{
              //   backgroundColor: 'red',
              backgroundColor: 'white',
              width: '88%',
              borderRadius: 5,
              elevation: 10,

              alignSelf: 'center',

              flexDirection: 'row',
            }}>
            <Icon
              containerStyle={{padding: 10}}
              type="font-awesome"
              name="search"
              color="gray"
              size={20}></Icon>
            <TextInput
              placeholder="Search message"
              underlineColorAndroid="transparent"></TextInput>
          </View>
          <ScrollView style={{padding: 10}}>
            {listViewData.map((l, i) => (
              <ListItem
                key={i}
                leftAvatar={
                  <Avatar
                    containerStyle={{alignSelf: 'center', marginBottom: 5}}
                    rounded
                    size={50}
                    source={{
                      uri: l.profile_photo,
                    }}
                    renderPlaceholderContent={
                      <Icon
                        type="material"
                        name="account-box"
                        color="#000"
                        // size={60}
                      />
                    }
                  />
                }
                title={l.name}
                subtitle={Services.TextAbstract(l.content, 80)}
                onPress={() => {
                  this.props.navigation.navigate('MessageScreen', {
                    fromChat: true,
                    data: l,
                  });
                }}
              />
            ))}
          </ScrollView>

          {/* <View
            style={{
              position: "absolute",
              right: 30,
              bottom: 30
            }}
          >
            <Icon
              type="material"
              name="add-circle"
              size={50}
              color="black"
              onPress={() => {
                this._onPressButton();
              }}
            />
          </View> */}
        </View>
      </View>
    );
  }
}

export default ChatListScreen;
