'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  Animated,
  TouchableOpacity,
  Dimensions,
  Alert,
  FlatList,
} from 'react-native';
import {
  Header,
  Icon,
  Button,
  ListItem,
  Avatar,
  Card,
  Image,
  Divider,
} from 'react-native-elements';
import styles from '../styles/Stylesheet';
// import Ionicons from 'react-native-vector-icons/Ionicons';

// import {
// 	// createMaterialTopTabNavigator,
// 	MaterialTopTabBar,
// 	createAppContainer
// } from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
// import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';

import * as Services from '../services/Services';
// import {tsBigIntKeyword} from '@babel/types';

var savingData = false;
var processingData = false;

const data = [
  {
    imageUrl: '',
    title: 'something',
  },
  {
    imageUrl: 'http://via.placeholder.com/160x160',
    title: 'something two',
  },
  {
    imageUrl: 'http://via.placeholder.com/160x160',
    title: 'something three',
  },
  {
    imageUrl: 'http://via.placeholder.com/160x160',
    title: 'something four',
  },
  {
    imageUrl: 'http://via.placeholder.com/160x160',
    title: 'something five',
  },
  {
    imageUrl: 'http://via.placeholder.com/160x160',
    title: 'something six',
  },
];
class TempScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
          <Text>Feature coming soon!</Text>
        </View>
      </View>
    );
  }
}
class InformationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fieldsVal: [],
      newdata: [],
    };
  }
  componentDidMount() {
    const users = this.props.navigation.getParam('data');
    console.log('dvdvvdvdvd', users);
    const aydi = users.key ? users.key : users.id;
    console.log(aydi + '    ALL DETAILS');
    Services.getUsers().then(res => {
      const newData = res.filter(item => {
        const ID = item.id;
        if (ID == aydi) {
          this.setState({newdata: item});
        }
      });
    });
  }

  render() {
    console.log(this.state.newdata);
    const thisData = this.state.newdata;
    const comp = thisData.companies;
    const schools = thisData.schools;
    return (
      <View style={{flex: 1}}>
        {/* <View style={{flex: 1, justifyContent: 'flex-start', padding: 20}}>
          <Text> First Name : {thisData.first_name}</Text>
          <Text> Middle Name : {thisData.middle_name}</Text>
          <Text> Last Name : {thisData.last_name} </Text>
          <Text> Email : {thisData.email}</Text>
          <Text> Gender : {thisData.gender}</Text>
          <Text> Contact Number : {thisData.contact_number}</Text>
          <Text> Occupation : {thisData.occupation} </Text>
          <Text> Companies : {comp ? comp[0].name : ''}</Text>
          <Text> Schools : {schools ? schools[0].name : ''}</Text>
          <Text> Website : {thisData.website}</Text>
          <Text> Date Joined : {thisData.date_joined} </Text>
        </View> */}
      </View>
    );
  }
}

const TabNavigator = createMaterialTopTabNavigator(
  {
    Information: {
      screen: InformationScreen,
      navigationOptions: {
        header: null,
        tabBarIcon: ({tintColor}) => (
          <Icon name="bookmark" color={tintColor} size={24} />
        ),
      },
    },
    Posts: {
      screen: TempScreen,
      navigationOptions: {
        header: null,

        tabBarIcon: ({tintColor}) => (
          <Icon name="bookmark" color={tintColor} size={24} />
        ),
      },
    },
  },
  {
    navigationOptions: ({navigation, screenProps}) => ({
      header: null,
      headerMode: 'none',
      tabBarVisible: true,

      // tabBarLabel: () => {
      //   const { routeName } = navigation.state;
      //   switch (routeName) {
      //     //
      //   }
      //   return <Text>{routeName}</Text>;
      // },
    }),
    animationEnabled: true,
    swipeEnabled: false,
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'black',
      indicatorStyle: {
        backgroundColor: 'white',
      },
      labelStyle: {
        // fontSize: 14 * ratio,
        color: 'black',
        fontStyle: 'normal',
      },
      tabStyle: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
      },
      style: {
        backgroundColor: 'gray',
      },

      // statusBarStyle: "light-content"
    },
  },
);
// const AppTabContainer = createAppContainer(TabNavigator);

class ProfileScreen extends React.Component {
  static router = TabNavigator.router;

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      data: data,
      newdata: [],
    };
  }

  componentDidMount() {
    const user = this.props.navigation.getParam('data');
    console.log(user, 'dito kana po');
    const aydi = user.key ? user.key : user.id;
    console.log(aydi + '    ID NIYA');
    this.setState({
      user: user,
    });
    Services.getUsers().then(res => {
      const newData = res.filter(item => {
        const ID = item.id;
        if (ID == aydi) {
          this.setState({newdata: item});
        }
        // this.props.navigation.setParams({something:"something"});
      });
    });
  }

  _onPressButton = () => {
    const item = this.state.newdata;

    // props.item.viewed = true;
    this.setState({}, () => {
      this.props.navigation.navigate('ProfileEditScreen', {
        data: item,
      });
    });
  };

  render() {
    const config = {
      velocityThreshold: 0.5,
      directionalOffsetThreshold: 40,
    };
    console.log('HERE WE GO', this.state.newdata);
    const thisData = this.state.newdata;

    // console.log("TEST", this.props.navigation.getParam("something"));

    return (
      <View style={{flex: 1, backgroundColor: 'gainsboro'}}>
        <View
          style={{
            backgroundColor: '#009c9e',
            flexDirection: 'row',
          }}>
          <View style={{justifyContent: 'center', padding: 10}}>
            <Icon
              type="material"
              name="arrow-back"
              color="#fff"
              size={30}
              onPress={() => {
                this.props.navigation.goBack();
              }}
            />
          </View>
          <View style={{flex: 1, justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#fff', fontSize: 20}}>
              {thisData.first_name + "'s Profile"}
            </Text>
          </View>
          <View style={{justifyContent: 'center', padding: 20}}>
            <Icon type="material" name="settings" color="#fff" size={30} />
          </View>
        </View>
        <View style={{flex: 1}}>
          {/* <View style={{height: 20}} /> */}
          <View style={{flex: 1}}>
            <View
              style={{
                flex: 3,
                backgroundColor: 'white',
                width: '95%',
                borderRadius: 50,

                elevation: 10,
                alignSelf: 'center',
                justifyContent: 'center',
                height: '100%',
              }}>
              <View
                style={{
                  flex: 1,
                }}>
                <View style={{height: 20}}></View>
                <Avatar
                  containerStyle={{
                    alignSelf: 'center',
                    // marginTop: 1,
                    // borderWidth: 2,
                    // borderColor: 'black',
                  }}
                  rounded
                  size={120}
                  source={{
                    uri: thisData.profile_photo,
                  }}
                />
                <Text style={styles.TextBoldCenter}>{thisData.first_name}</Text>
                <Text style={styles.TextDefaultCenter}>
                  {'@' + thisData.handle_name}
                </Text>
              </View>
              <View height={100}></View>
              {/* <Divider style={{width: '90%', alignSelf: 'center'}}></Divider> */}
              <View style={{flex: 1}}>
                <View
                  style={{
                    backgroundColor: 'white',
                    width: '80%',
                    borderRadius: 10,
                    elevation: 10,
                    alignSelf: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      height: '70%',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <View style={{flex: 1}}>
                      <Text style={styles.TextDefaultCenter}>Posts</Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          color: '#009c9e',
                          fontWeight: 'bold',
                        }}>
                        12
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <View>
                        <Text style={styles.TextDefaultCenter}>Followers</Text>
                        <Text
                          style={{
                            textAlign: 'center',
                            color: '#009c9e',
                            fontWeight: 'boldss',
                          }}>
                          45
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={styles.TextDefaultCenter}>Following</Text>
                      <Text
                        style={{
                          textAlign: 'center',
                          color: '#009c9e',
                          fontWeight: 'bold',
                        }}>
                        50
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={{flex: 3}}>
              <View style={{width: '100%', flex: 1}}>
                {/* <TabNavigator navigation={this.props.navigation} /> */}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default ProfileScreen;
