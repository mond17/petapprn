'use strict';

import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  TextInput,
  StatusBar,
} from 'react-native';
import {Header, Icon, ListItem, Avatar, Input} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import ScalableText from 'react-native-text';
import * as Services from '../services/Firebase_Services';

function _onSubmit() {
  // const res =
  Services.SignInUser();

  // console.log('RESULT :', res);

  // console.lg("You are login!")

  // })
}
function LogInScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <StatusBar backgroundColor="#009c9e" barStyle="light-content" />

      {/* <View
          style={{
            flex: 2,

            alignItems: 'center',
          }}>
          <Image
            style={{
              width: '70%',
              height: '70%',
              marginTop: 50,
              borderRadius: 40,
            }}
            source={require('../sources/petapp_logo.png')}
            // resizeMode="contain"
            // resizeMode="contain"
          />

          <ScalableText
            style={{
              flex: 1,
              padding: 10,

              // alignSelf: 'center',
              fontFamily: 'afternoon_in_stereo_personal_use',
              fontSize: 70,
              marginTop: 290,
              position: 'absolute',
            }}>
            Pet app
          </ScalableText>
        </View>
        
         */}
      <View style={{flex: 1, padding: 30, alignSelf: 'center'}}>
        <View
          style={{
            backgroundColor: 'gainsboro',
            borderRadius: 25,
            padding: 5,
          }}>
          <TextInput
            underlineColorAndroid="transparent"
            height={40}
            paddingLeft={10}
            placeholder="Email address"
          />
        </View>
        <View style={{height: 10}} />
        <View
          style={{
            backgroundColor: 'gainsboro',
            borderRadius: 25,
            padding: 5,
          }}>
          <TextInput
            underlineColorAndroid="transparent"
            height={40}
            paddingLeft={10}
            placeholder="Password"
          />
        </View>
        <View style={{height: 10}} />
        <TouchableOpacity
          style={{
            height: 50,
            marginTop: 10,
            backgroundColor: 'lightseagreen',
            borderRadius: 10,
          }}
          onPress={() => {
            _onSubmit();
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
              LOG IN
            </Text>
          </View>
        </TouchableOpacity>
        <View style={{height: 10}} />
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <View style={{height: 15}} />
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('RegisterScreen');
            }}>
            <Text style={{color: 'lightseagreen'}}>Don't have an account?</Text>
          </TouchableOpacity>
          <View style={{height: 10}} />
          <TouchableOpacity>
            <Text style={{color: 'lightseagreen'}}>
              Forgot Password? Click HERE!
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default LogInScreen;
