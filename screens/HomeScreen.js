'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  StatusBar,
  FlatList,
} from 'react-native';
import {
  Button,
  Input,
  Overlay,
  Icon,
  Header,
  Test,
  Avatar,
} from 'react-native-elements';
import styles from '../styles/Stylesheet';

import NumberInput from '../components/NumberInput';

import CardScreen from '../components/CardScreen';
import ScalableText from 'react-native-text';
import * as Services from '../services/Services';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: {},
      profile_photo: '',
    };
  }

  componentDidMount() {
    console.log('HEREEEEEEEEE!', this.props);
    Services.getUsers().then(res => {
      this.users = res;
      const newData = Services.prepDummyData(this.users, 10);

      console.log('popopopop ETO NA', this.state.listViewData);

      const newUser =
        'https://picsum.photos/id/' +
        Services.getRandomInt(0, 100) +
        '/100/100';
      this.setState({
        listViewData: newData,
        profile_photo: newData[1].profile_photo,
      });
    });
  }
  render() {
    const array = this.state.listViewData;
    console.log('wowowow ETO NA', array);
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#009c9e" barStyle="light-content" />

        <View
          style={{
            backgroundColor: '#009c9e',
            padding: 15,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Icon
              style={{paddingStart: 10}}
              type="font-awesome"
              name="camera-retro"
              color="#fff"
              size={30}
              // onPress={() => {

              // }}
            />
            {/* <View style={{flex: 1, alignItems: 'center'}}>
              <ScalableText
                style={{
                  padding: 2,

                  // alignSelf: 'center',
                  color: '#ffff',
                  fontFamily: 'afternoon_in_stereo_personal_use',
                  fontSize: 50,
                  // marginTop: 290,
                }}>
                Pet app
              </ScalableText>
            </View> */}

            <Avatar
              containerStyle={{alignSelf: 'center', marginBottom: 5}}
              rounded
              size={40}
              source={{
                uri: Services.userData.profile_photo,
              }}
              renderPlaceholderContent={
                <Icon
                  type="material"
                  name="account-circle"
                  color="#fff"
                  size={35}
                />
              }
              onPress={() => {
                this.props.navigation.toggleDrawer();
              }}
            />
          </View>
        </View>
        <ScrollView>
          <View style={{height: 20}} />

          <View>
            <FlatList
              data={array}
              renderItem={(data, rowMap) => (
                <CardScreen
                  sporp={this.props}
                  id={data.item.key}
                  profile_photo={data.item.profile_photo}
                  name={data.item.name}
                  handle={data.item.handle}
                  likes={data.item.likes}
                  comments={data.item.comments}
                  shares={data.item.shares}
                  content={data.item.content}
                  image={data.item.image}
                />
              )}
            />
          </View>
          <View style={{height: 20}} />
        </ScrollView>
      </View>
    );
  }
}

export default HomeScreen;
