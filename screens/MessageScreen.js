'use strict';

import React from 'react';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
} from 'react-native';
import {Header, Icon, Input} from 'react-native-elements';
import {GiftedChat} from 'react-native-gifted-chat';

import styles from '../styles/Stylesheet';

import * as Services from '../services/Services';
import {pctDecChars} from 'uri-js';

const list = [
  {
    name: 'Amy Farha',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President',
  },
  {
    name: 'Chris Jackson',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman',
  },
  {
    name: 'Donald Trump',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'President',
  },
];

var interval;

class ChatContainer extends React.Component {
  render() {
    const position = this.props.me == 'me' ? 'flex-end' : 'flex-start';
    const bgColor = this.props.me == 'me' ? 'blue' : '#ccc';
    const fontColor = this.props.me == 'me' ? '#fff' : '#000';

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: position,
          alignItems: 'flex-end',
          width: '100%',
          paddingLeft: 5,
        }}>
        {this.props.me != 'me' && (
          <Icon
            type="material"
            name="account-circle"
            color="#000"
            size={40}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          />
        )}
        <View
          style={{
            height: 'auto',
            width: 'auto',
            maxWidth: '60%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: bgColor,
            padding: 15,
            borderRadius: 20,
            flexWrap: 'wrap',
            flexShrink: 1,
            margin: 5,
          }}>
          <Text style={{color: fontColor, flexWrap: 'wrap'}}>
            {this.props.message}
          </Text>
        </View>
      </View>
    );
  }
}

class MessageScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chatHistory: [],
      chatboxValue: '',
      chatboxValue: '',
      messages: [],
      listViewChat: [],
      user: {},
      textValue: '',
      picValue: '',
      counterMessage: 0,
      keyValue: {},
    };
  }

  componentDidMount() {
    const chatHistory = this.state.chatHistory;

    const userdata = this.props.navigation.getParam('data');

    console.log(userdata);

    this.setState({
      messages: [
        {
          _id: 0,
          text: 'Hello Monde!',
          createdAt: new Date(),
          user: {
            name: userdata.name,
            avatar: userdata.profile_photo,
            data: userdata,
          },
        },
      ],
      user: userdata,
      keyValue: userdata,
    });

    Services.getUsers().then(res => {
      const quotes = Services.getQuotes();
      this.users = res;
      const newChat = Services.prepDummyData(this.users, 10);
      this.setState({listViewChat: newChat, quotes: quotes});

      // console.log("ETO NGA",newChat);
    });
  }

  componentWillUnmount() {
    clearInterval(interval);
  }

  _onPressButton = () => {
    console.log('pressed!');
    alert('pressed!');
  };

  rep = () => {
    // console.log(newData);
    // const messages =this.state.messages;

    console.log('ID HERE   ', this.state.counterMessage);

    const {user, quotes} = this.state;
    console.log('rep');
    setTimeout(() => {
      console.log('inside');
      // -      chatHistory.push({ sender: "notme", message: "some message" });
      // -      this.setState({ chatHistory: chatHistory });

      Services.getUsers().then(res => {
        const quotes = Services.getQuotes();
        // const data = Services.prepDummyData(res, quotes.length - 1);
        const multiplier = Services.getRandomInt(0, 100);
        var media = {};
        if (multiplier >= 50) {
          media = {
            textValue:
              quotes[Services.getRandomInt(0, quotes.length - 1)].quote,
            picValue: '',
          };
        } else {
          media = {
            textValue: '',
            picValue:
              'https://picsum.photos/id/' +
              Services.getRandomInt(0, 100) +
              '/' +
              Dimensions.get('window').width,
          };
        }
        // console.log(user + 'DITO KANA HGAHAHA');
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, {
            _id: previousState.messages.length,
            text: media.textValue,
            image: media.picValue,
            // video: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
            createdAt: new Date(),
            user: {
              _id: parseInt(user.key),
              name: user.name,
              avatar: user.profile_photo,
            },
          }),
        }));
      });
    }, Services.getRandomInt(1, 5) * 500);
  };

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));

    this.rep();
  }

  render() {
    const chatName = this.state.user.name;
    var behavior = '';

    // console.log('lopbnbnb', this.state.user);
    if (Platform.OS == 'ios') {
      behavior = 'padding';
    }

    return (
      <View style={{flex: 1}}>
        <View
          style={{
            backgroundColor: '#009c9e',
            flexDirection: 'row',
            flex: 0.1,
          }}>
          <View style={{justifyContent: 'center', padding: 10}}>
            <Icon
              type="material"
              name="arrow-back"
              color="#fff"
              size={30}
              onPress={() => {
                !this.props.navigation.getParam('fromChat')
                  ? this.props.navigation.replace('ChatFeatureScreen')
                  : this.props.navigation.goBack();
              }}
            />
          </View>
          <View style={{flex: 1, justifyContent: 'center', padding: 10}}>
            <Text style={{color: '#fff', fontSize: 20}}>{chatName}</Text>
          </View>
          <View style={{justifyContent: 'center', padding: 20}}>
            <Icon
              type="material"
              name="info"
              color="#fff"
              size={30}
              onPress={() => {
                this._goToMyProfileScreen();
              }}
            />
          </View>
        </View>
        <KeyboardAvoidingView style={{flex: 1}} behavior={behavior}>
          <GiftedChat
            // imageProps={resizeMode="cover"}
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            onPressAvatar={user => {
              this.props.navigation.navigate('MyProfileScreen', {
                data: this.state.keyValue,
              });
              // console.log(user);
            }}
            user={{
              _id: 1,
            }}></GiftedChat>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default MessageScreen;
