'use strict';

import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import {Button, Input, Overlay, Icon, Header} from 'react-native-elements';
import styles from '../styles/Stylesheet';
import {BoxShadow} from 'react-native-shadow';

import NumberInput from '../components/NumberInput';

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

var fields = [
  {fieldName: '', placeholder: '', errorMessage: ''},
  {fieldName: '', placeholder: '', errorMessage: ''},
  {fieldName: '', placeholder: '', errorMessage: ''},
  {fieldName: '', placeholder: '', errorMessage: ''},
];
var fakeEmails = ['core@gmail.com', 'qwerty@gmail.com'];

class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      termsVisibile: false,
      privacyPolicyVisible: false,
      fieldFN: '',
      fieldMN: '',
      fieldLN: '',
      fieldEmail: '',
      fieldMobile: '',
      fieldPW: '',
      fieldFNError: '',
      fieldMNError: '',
      fieldLNError: '',
      fieldEmailError: '',
      fieldMobileError: '',
      fieldPWError: '',
    };
  }

  _onChangeText(field, value) {
    console.log(field, value);
    const temp = {};
    temp[field] = value;
    this.setState(temp);
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validateEmail2(email) {
    if (fakeEmails.includes(email)) {
      console.log('in ' + email);
      return true;
    }
    console.log('out');
    return false;
  }
  validatePhoneNumber(phoneNumber) {
    // var regExp = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}/;
    var phone = phoneNumber.length;
    if (phone == 9) {
      return true;
    }
    return false;
  }
  validatePassword(pass) {
    var passLength = pass.length;
    if (passLength == 8) {
      return true;
    }
    return false;
  }

  _onPressButton = () => {
    const fields = [
      'fieldFN',
      'fieldMN',
      'fieldLN',
      'fieldEmail',
      'fieldMobile',
      'fieldPW',
    ];

    var temp = {};
    var score = 0;

    for (let i = 0; i < fields.length; i++) {
      const value = this.state[fields[i]];
      console.log(fields[i], ': ', value, value == '');

      // console.log(tmp);
      // console.log(fields[i]);

      if (fields[i] == 'fieldEmail') {
        // console.log(fields[i]);
        // console.log(tmp);
        if (this.validateEmail(value)) {
          temp[fields[i] + 'Error'] = '';
        } else {
          temp[fields[i] + 'Error'] = 'Email is not acceptable';
          score++;
        }

        if (this.validateEmail2(value)) {
          temp[fields[i] + 'Error'] = 'Email is already exist';
          score++;
        }
      } else if (fields[i] == 'fieldMobile') {
        if (this.validatePhoneNumber(value)) {
          temp[fields[i] + 'Error'] = '';
        } else {
          temp[fields[i] + 'Error'] = 'Mobile number is incorrect ';
          score++;
        }
      } else if (fields[i] == 'fieldPW') {
        if (this.validatePassword(value)) {
          temp[fields[i] + 'Error'] = '';
        } else {
          temp[fields[i] + 'Error'] = 'Your password is too short ';
          score++;
        }
      } else if (value == '') {
        temp[fields[i] + 'Error'] = 'This field cannot be blank';
        score++;
      } else {
        temp[fields[i] + 'Error'] = '';
      }
    }
    // {this.state.fieldEmail }

    this.setState(temp, () => {
      console.log(this.state);

      if (score == 0) {
        //do action
        // this.props.navigation.navigate("SignInScreen");
      }
    });

    // console.log(this.state);
    // for (const key in this.state) {
    //   console.log(key, this.state[key]);
    // }

    // this.props.navigation.navigate("SignInScreen");
  };

  _signInButton = () => {
    // this.props.navigation.navigate("SignInScreen");
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 1,
          }}>
          <View
            style={{
              flex: 1,
              // backgroundColor: 'red',
              // padding: 0,
              alignItems: 'center',
            }}>
            {/* <Text>WOWOWOWOWOOWOIWIWIWIWOW</Text> */}
            <Image
              style={{
                width: '80%',
                height: '80%',
                marginTop: 50,
                // borderRadius: 40,
              }}
              source={require('../sources/userphoto.png')}
              resizeMode="contain"
              // resizeMode="contain"
            />
          </View>
          {/* <ScrollView> */}
          <View
            style={{
              flex: 3,
              padding: 50,
            }}>
            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 20,
                paddingLeft: 10,
                padding: 5,

                // overflow: hidden,
              }}>
              <TextInput
                underlineColorAndroid="transparent"
                height={40}
                placeholder="Full Name"
                onChangeText={this._onChangeText.bind(this, 'fieldLN')}
                errorMessage={this.state.fieldLNError}></TextInput>
            </View>

            <View style={{height: 10}}></View>
            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 20,
                paddingLeft: 10,
                padding: 5,
              }}>
              <TextInput
                underlineColorAndroid="transparent"
                height={40}
                placeholder="User Name"
                onChangeText={this._onChangeText.bind(this, 'fieldLN')}
                errorMessage={this.state.fieldLNError}></TextInput>
            </View>
            <View style={{height: 10}}></View>

            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 20,
                paddingLeft: 10,
                padding: 5,
              }}>
              <TextInput
                underlineColorAndroid="transparent"
                height={40}
                placeholder="Email address"
                onChangeText={this._onChangeText.bind(this, 'fieldEmail')}
                errorMessage={this.state.fieldEmailError}></TextInput>
            </View>
            <View style={{height: 10}}></View>

            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 20,
                paddingLeft: 5,
              }}>
              <Input
                inputContainerStyle={{
                  borderBottomWidth: 0,
                  placeholder: 'xx-xxx-xxxx',
                }}
                // underlineColorAndroid="gainsboro"
                height={40}
                inputComponent={NumberInput}
                leftIcon={<Text>+639</Text>}
                placeholder="xx-xxx-xxxx"
                onChangeText={this._onChangeText.bind(this, 'fieldMobile')}
                errorMessage={this.state.fieldMobileError}></Input>
            </View>
            <View style={{height: 10}}></View>

            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 15,
                borderRadius: 20,
                paddingLeft: 10,
                padding: 5,
              }}>
              <TextInput
                underlineColorAndroid="transparent"
                height={40}
                placeholder="Password"
                onChangeText={this._onChangeText.bind(this, 'fieldPW')}
                errorMessage={this.state.fieldPWError}></TextInput>
            </View>
            <View style={{height: 10}}></View>
            <View
              style={{
                backgroundColor: '#f6f6f6',
                borderRadius: 15,
                borderRadius: 20,
                paddingLeft: 10,
                padding: 5,
              }}>
              <TextInput
                underlineColorAndroid="transparent"
                height={40}
                placeholder="Confirm Password"
                onChangeText={this._onChangeText.bind(this, 'fieldPW')}
                errorMessage={this.state.fieldPWError}></TextInput>
            </View>

            <View style={{height: 30}} />
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                height: 50,
                width: 150,
                marginTop: 10,
                backgroundColor: 'lightseagreen',
                borderRadius: 30,
              }}
              onPress={() => {
                this.props.navigation.navigate('HomeScreen');
              }}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{fontSize: 20, color: 'white'}}>SIGN UP</Text>
              </View>
            </TouchableOpacity>
            <View style={{height: 20}} />

            {/* <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                justifyContent: "center"
              }}
            >
              <Text>You agree to </Text>
              <TouchableOpacity
                onPress={() => this.setState({ termsVisibile: true })}
              >
                <Text style={{ fontWeight: "bold" }}>User Agreement</Text>
              </TouchableOpacity>
              <Text>, </Text>
              <TouchableOpacity
                onPress={() => this.setState({ privacyPolicyVisible: true })}
              >
                <Text style={{ fontWeight: "bold" }}>Privacy Policy</Text>
              </TouchableOpacity>
              <Text>, and </Text>
              <TouchableOpacity
                onPress={() => this.setState({ termsVisibile: true })}
              >
                <Text style={{ fontWeight: "bold" }}>Cookie Policy</Text>
              </TouchableOpacity>
            </View>
            <View style={{ height: 20 }} />
            <View style={{ flexDirection: "row" }}>
              <Text>Already have an account? </Text>
              <TouchableOpacity
                onPress={() => {
                  this._signInButton();
                }}
              >
                <Text style={{ fontWeight: "bold" }}>Sign In</Text>
              </TouchableOpacity>
            </View> */}
          </View>
          {/* </ScrollView> */}
        </View>

        {/* <Overlay
          isVisible={this.state.termsVisibile}
          onBackdropPress={() => this.setState({ termsVisibile: false })}
        >
          <View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text style={styles.TextBoldLeft}>Terms and Conditions</Text>
              <Icon
                type="material"
                name="close"
                size={30}
                onPress={() => {
                  this.setState({ termsVisibile: false });
                }}
              />
            </View>
            <ScrollView>
              <View style={{ padding: 10 }}>
                <Text>{lorem}</Text>
              </View>
            </ScrollView>
          </View>
        </Overlay>
        <Overlay
          isVisible={this.state.privacyPolicyVisible}
          onBackdropPress={() => this.setState({ privacyPolicyVisible: false })}
        >
          <View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text style={styles.TextBoldLeft}>Privacy Policy</Text>
              <Icon
                type="material"
                name="close"
                size={30}
                onPress={() => {
                  this.setState({ privacyPolicyVisible: false });
                }}
              />
            </View>
            <ScrollView>
              <View style={{ padding: 10 }}>
                <Text>{lorem}</Text>
              </View>
            </ScrollView>
          </View>
        </Overlay> */}
      </View>
    );
  }
}
const shadowOpt = {
  width: 100,
  height: 100,
  color: '#000',
  border: 2,
  radius: 3,
  opacity: 0.2,
  x: 0,
  y: 3,
  style: {marginVertical: 5},
};

export default RegisterScreen;
