'use strict';

import React from 'react';
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  SectionList,
  ListView,
  Alert,
  StyleSheet,
  TextInput,
  StatusBar,
} from 'react-native';
import {
  Header,
  Icon,
  ListItem,
  Avatar,
  Input,
  Divider,
} from 'react-native-elements';
import {ScrollView, FlatList} from 'react-native-gesture-handler';
import {Card} from 'react-native-paper';
import ScalableText from 'react-native-text';
import styles from '../styles/Stylesheet';
import * as Services from '../services/Services';
const userdata = [
  {
    name: 'Location',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President',
  },
  {
    name: 'Chris Jackson',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman',
  },
  {
    name: 'Location',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President',
  },
  {
    name: 'Chris Jackson',
    avatar_url:
      'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman',
  },
];
class CardScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userdata: {},
      likes: props.likes,
      comments: props.comments,
      shares: props.shares,
      profile_photo: props.profile_photo,
    };
  }

  componentDidMount() {}
  checkAccount(value) {
    console.log('jkjkjkjk', value.sporp);
    const navigation = value.sporp.navigation;
    console.log('GGFGFGFG', value);
    navigation.navigate('ProfileScreen', {
      data: value,
    });
  }
  render() {
    const state = this.state;
    return (
      <View style={{flex: 1}}>
        <View style={{height: 10}}></View>
        <View
          style={{
            backgroundColor: 'white',
            width: '95%',
            borderRadius: 35,
            elevation: 10,
            alignSelf: 'center',
            justifyContent: 'center',
          }}>
          <View style={{padding: 10, flexDirection: 'row'}}>
            <Avatar
              rounded
              source={{
                uri: this.props.profile_photo,
              }}
              size={50}
              onPress={() => {
                this.checkAccount(this.props);
              }}
              renderPlaceholderContent={
                <Icon type="material" name="account-box" color="#000" />
              }></Avatar>

            <View style={{padding: 10, flex: 1}}>
              <Text style={{fontWeight: 'bold'}}> {this.props.name}</Text>
              <Text> {this.props.handle}</Text>
            </View>
            <View style={{padding: 10}}>
              <Icon
                type="font-awesome"
                name="ellipsis-v"
                color="#000"
                size={25}
              />
            </View>
          </View>

          <Divider></Divider>

          {this.props.image && (
            <View
              style={{
                padding: 0,
                alignItems: 'center',
                justifyContent: 'center',
                height: 200,
              }}>
              <Image
                source={{uri: this.props.image}}
                style={{width: '100%', height: '100%'}}
              />
            </View>
          )}
          <View style={{padding: 10}}>
            <Text>{this.props.content}</Text>
          </View>

          <Divider></Divider>
          <View
            style={{
              padding: 10,
              flexDirection: 'row',
            }}>
            <View style={{}}>
              <TouchableOpacity activeOpacity={0.5}>
                <Image
                  style={{marginStart: 10, width: 30, height: 30}}
                  resizeMode="contain"
                  source={require('../sources/red_heart.png')}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                paddingStart: 5,
                alignSelf: 'center',
              }}>
              <Text>{this.state.likes}</Text>
            </View>
            <View style={{paddingStart: 20}}>
              <TouchableOpacity activeOpacity={0.5}>
                <Icon
                  type="font-awesome"
                  name="comment"
                  // color="#"
                  size={25}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                paddingStart: 5,
                alignSelf: 'center',
              }}>
              <Text>{this.state.comments}</Text>
            </View>
          </View>
        </View>
        <View style={{height: 10}}></View>
      </View>
    );
  }
}

export default CardScreen;
