'use strict';

import React from 'react';
import {TextInput} from 'react-native';

class NumberInput extends React.Component {
  _onChanged(text) {
    this.setState(
      {
        mobile: text.replace(/[^0-9]/g, ''),
      },
      () => {
        this.props.onChangeText(this.state.mobile);
      },
    );
  }

  render() {
    return (
      <TextInput
        style={{width: '100%'}}
        keyboardType="numeric"
        onChangeText={text => this._onChanged(text)}
        // value={this.state.myNumber}
        maxLength={10} //setting limit of input
      />
    );
  }
}

export default NumberInput;
