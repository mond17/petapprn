// In App.js in a new project

import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import * as Services from './services/Firebase_Services';
import styles from './styles/Stylesheet';

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
//screens
import HomeScreen from './screens/HomeScreen';
import RegisterScreen from './screens/RegisterScreen';
import LogInScreen from './screens/LogInScreen';
import ProfileScreen from './screens/ProfileScreen';
import CardScreen from './components/CardScreen';
import ExploreMainScreen from './screens/ExploreMainScreen';
import MessageScreen from './screens/MessageScreen';
import ChatListScreen from './screens/ChatListScreen';
import {useNavigation} from '@react-navigation/native';

import auth from '@react-native-firebase/auth';

function SplashScreen() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const navigation = useNavigation();
  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;
  user ? navigation.navigate('HomeScreen') : navigation.navigate('LogInScreen');

  return (
    <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
      <Text style={{alignSelf: 'center'}}>Welcome MOND</Text>
    </View>
  );
}

function TempScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
      <Text style={{alignSelf: 'center'}}>Feature coming soon!</Text>
    </View>
  );
}
function CustomDrawerContent(props) {
  const {progress, navigation} = props;

  const translateX = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [-80, 0],
  });

  // useFocusEffect(
  //   React.useCallback(() => {
  //     authStore._onQueryMe();
  //   }, []),
  // );
  return (
    <DrawerContentScrollView {...props}>
      <Animated.View style={{transform: [{translateX}]}}>
        <View
          style={{
            flex: 1,
            paddingTop: 20,
          }}>
          <View
            style={{
              backgroundColor: 'white',
              borderTopRightRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <View
              style={[
                {padding: 20, paddingTop: 40, justifyContent: 'flex-start'},
              ]}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                {/* <Image
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 15,
                      borderWidth: 0.3,
                      borderColor: 'white',
                    }}
                    resizeMode={'cover'}
                    source={
                      authStore.profile_photo
                        ? {uri: authStore.profile_photo}
                        : require('./assets/icons/Profile.png')
                    }
                    color={styles.Color1}
                  /> */}

                <TouchableOpacity
                  style={{right: 0}}
                  onPress={() => {
                    navigation.closeDrawer();
                  }}>
                  {/* <Image
                    style={{width: 30, height: 30}}
                    source={require('./assets/icons/menu.png')}
                    color="#EBC686"
                  /> */}
                </TouchableOpacity>
              </View>
              <Text
                style={[
                  styles.textBoldColor,
                  {fontSize: 20, fontWeight: 'bold', paddingTop: 10},
                ]}>
                {/* {authStore.first_name + ' ' + authStore.last_name} */}
              </Text>
              {/* <Text style={styles.textInputGray}>{authStore.position}</Text> */}
            </View>
            <View
              style={[
                {
                  width: '100%',
                  height: 0.8,
                  marginBottom: 20,
                  backgroundColor: '#EBC686',
                },
              ]}
            />
            <DrawerItemList {...props} />
            <DrawerItem
              label="Privacy Policy"
              //   labelStyle={styles.textInputGray}
              onPress={
                () => {}
                //   navigation.navigate('WebViewScreen', {
                //     url: 'https://xwipecard.com/privacy_policy.html',
                //   })
              }
            />
            <DrawerItem
              label="Logout"
              labelStyle={{color: 'white'}}
              onPress={() => {
                Services.LogoutUser();
              }}
            />
            {/* <DrawerItem
              label="Delete Account"
              labelStyle={styles.textInputGray}
              onPress={() => alert('You clicked Delete Account')}
            /> */}
          </View>
        </View>
      </Animated.View>
    </DrawerContentScrollView>
  );
}
function DrawerNavigator() {
  return (
    <Drawer.Navigator
      drawerType="front"
      // lazy="false"
      drawerContentOptions={{
        activeTintColor: '#EBC686',
        inactiveTintColor: '#fff',
        inactiveBackgroundColor: '#1C1B1F',
        activeBackgroundColor: '#1C1B1F',
        labelStyle: [styles.textInputWhite, {fontSize: 18, margin: 0}],
        itemStyle: {
          marginVertical: 0,
        },
      }}
      drawerStyle={{backgroundColor: 'transparent'}}
      drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        //   options={{
        //     title: 'Home',
        //     drawerIcon: ({focused, tintColor}) => (
        //       <Image
        //         style={{width: 35, height: 35}}
        //         source={
        //           focused
        //             ? require('./assets/icons/Menu-home.png')
        //             : require('./assets/icons/Menu-home.png')
        //         }
        //       />
        //     ),
        //   }}
      />
      <Drawer.Screen
        name="Profile"
        component={TempScreen}
        //   options={{
        //     title: 'My Profile',
        //     drawerIcon: ({focused, tintColor}) => (
        //       <Image
        //         style={{width: 35, height: 35}}
        //         source={
        //           focused
        //             ? require('./assets/icons/Menu-MyProfile.png')
        //             : require('./assets/icons/Menu-MyProfile.png')
        //         }
        //       />
        //     ),
        //   }}
      />

      <Drawer.Screen
        name="Settings"
        component={TempScreen}
        //   options={{
        //     title: 'Settings',
        //     drawerIcon: ({focused, tintColor}) => (
        //       <Image
        //         style={{width: 35, height: 35}}
        //         source={
        //           focused
        //             ? require('./assets/icons/menu-settings.png')
        //             : require('./assets/icons/menu-settings.png')
        //         }
        //       />
        //     ),
        //   }}
      />
      <Drawer.Screen
        name="Faqs"
        component={TempScreen}
        //   options={{
        //     title: 'FAQs',
        //     drawerIcon: ({focused, tintColor}) => (
        //       <Image
        //         style={{width: 35, height: 35}}
        //         source={
        //           focused
        //             ? require('./assets/icons/Menu-faqs.png')
        //             : require('./assets/icons/Menu-faqs.png')
        //         }
        //       />
        //     ),
        //   }}
      />
    </Drawer.Navigator>
  );
}

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          animationEnabled: false,
        }}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />

        <Stack.Screen name="LogInScreen" component={LogInScreen} />
        <Stack.Screen name="HomeScreen" component={DrawerNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
